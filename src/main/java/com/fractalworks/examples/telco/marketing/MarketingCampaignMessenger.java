/*
 * Copyright 2020-present FractalWorks Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.fractalworks.examples.telco.marketing;

import com.fractalworks.streams.core.data.streams.Context;
import com.fractalworks.streams.core.data.streams.StreamEvent;
import com.fractalworks.streams.processors.geospatial.structures.refdata.GeoFenceConstants;
import com.fractalworks.streams.sdk.functions.EventFunction;

import java.util.UUID;

/**
 * Simple marketing messenger event function
 *
 * @author Lyndon Adams
 */
public class MarketingCampaignMessenger implements EventFunction {

    private final UUID uuid = UUID.randomUUID();
    public static String MARKETING_MSG_FIELD = "MarketingCampaignMessenger";

    public MarketingCampaignMessenger() {
        // Required constructor
    }

    @Override
    public void onEvent(StreamEvent event, Context context) {
        GeoFenceConstants state = (GeoFenceConstants) context.getValue("state");
        int geofenceId = (int) context.getValue("geofenceId");
        Object trackingTag = context.getValue("trackingTag");
        String message = null;

        switch( state){
            case ENTERED:
                message = String.format("Welcome to geofence %d", geofenceId);
                break;
            case DWELLING:
                message = String.format("Enjoy just ask from geofence %d",geofenceId);
                break;
            case EXITED:
                message = String.format("See you next time from geofence %d",geofenceId);
                break;
            default:
                break;
        }

        if( message!= null){
            event.addValue(uuid, MARKETING_MSG_FIELD, message);
        }
    }
}
